/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   PekinSOFT_Utils
 *  Class      :   MessageBox.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 8, 2020 @ 12:44:04 PM
 *  Modified   :   Mar 8, 2020
 *
 *  Purpose:
 *
 *  Revision History:
 *
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Mar 8, 2020  Sean Carrick        Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import java.awt.Component;
import javax.swing.JOptionPane;

/**
 * The {@code MessageBox} class provides static methods for displaying information to
 * the user.
 *
 * @author Sean Carrick &lt;PekinSOFT at outlook dot com&gt;
 *
 * @version 1.05
 * @since 0.1.0
 */
public class MessageBox {

    public static final int YES_OPTION = JOptionPane.YES_OPTION;
    public static final int NO_OPTION = JOptionPane.NO_OPTION;
    public static final int CANCEL_OPTION = JOptionPane.CANCEL_OPTION;
    public static final int OK_OPTION = JOptionPane.OK_OPTION;
    public static final int CLOSED_OPTION = JOptionPane.CLOSED_OPTION;

    private MessageBox() {
        /* No instantiation allowed. */ }

    /**
     * Standardized method of displaying informational messages to the user.
     * <p>
     * Messages displayed in this manner should not be for errors in the program
     * or warnings of improper usage. Furthermore, questions should not be asked
     * via this method, nor should input be sought. Again, only informational
     * messages should be displayed using this method.</p>
     * <p>
     * The {@code MessageBox.showInfo()} method uses the {@link java.util.ResourceBundle
     * ResourceBundle} to retrieve the {@code MessageBox}'s title prefix, and
     * adds to it the passed title for the message box. Therefore, the lead portion of
     * the title is already present by default, so only the message specifics
     * title portion needs to be provided. For example, if the message is
     * telling the user that changes were successfully saved, all that needs to
     * be provided to this message for the title is the value "Save Successful".
     * The {@code showInfo()} method is implemented as: </p>
     * <pre>
     * public static void showInfo(String message, String title) {
     *     JOptionPane.showMessageDialog(
     *             parent,
     *             message,
     *             bundle.getString("message.info.title")
     *             + title,
     *             JOptionPane.INFORMATION_MESSAGE); 
     * }
     * </pre>
     *
     * @param parent the component on which this {@code MessageBox} should be
     * centered
     * @param message The message to display to the user.
     * @param title The title of the message box. The {@code MessageBox} resources
     * already have "`${Application.name}` Information", so only the specifics
     * need to be provided.
     *
     */
    public static void showInfo(Component parent, String message, String title) {
        JOptionPane.showMessageDialog(
                parent,
                message,
                "Information: " + title,
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Standardized method of displaying warning messages to the user.
     * <p>
     * Messages shown in this manner should be used to warn users of a potential
     * problem, such as not providing required data, etc. Simple informational
     * messages should be shown using the showInfo method and errors should be
     * shown using the showError method. Questions and input should be sought
     * using the appropriate methods.</p>
     * <p>
     * The {@code MessageBox.showWarning()} method uses the {@link java.util.ResourceBundle
     * ResourceBundle} to retrieve the {@code MessageBox}'s title prefix, and
     * adds to it the passed title for the message box Therefore, the
     * lead portion of the title is already present by default, so only the
     * message specifics title portion needs to be provided. For example, if the
     * message is telling the user that changes were successfully saved, all
     * that needs to be provided to this message for the title is the value
     * "Save Successful". The {@code showWarning()} method is implemented as:</p>
     * <pre>
     * public static void showWarning(Component parent, String message, String title) {
     *     JOptionPane.showMessageDialog( 
     *             parent,
     *             message,
     *             bundle.getString("message.warning.title") + title,
     *             JOptionPane.INFORMATION_MESSAGE); 
     * }
     * </pre>
     *
     * @param parent the component on which this {@code MessageBox} should be
     * centered
     * @param message The message to display to the user.
     * @param title The title of the message box.
     *
     */
    public static void showWarning(Component parent, String message, String title) {
        JOptionPane.showMessageDialog(
                parent,
                message,
                "Warning: " + title,
                JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Standardized method of displaying errors to the user.
     * <p>
     * Error messages should be displayed from any {@code catch} block where the
     * {@code try} block has failed.</p>
     * <p>
     * The {@code MessageBox.showError()} method uses the {@link java.util.ResourceBundle
     * ResourceBundle} to retrieve the {@code MessageBox}'s title prefix, and
     * adds to it the passed title for the message box. Therefore, the
     * lead portion of the title is already present by default, so only the
     * message specifics title portion needs to be provided. For example, if the
     * message is telling the user that changes were successfully saved, all
     * that needs to be provided to this message for the title is the value
     * "Save Successful". The {@code showError()} method is implemented as:</p>
     * <pre>
     * public static void showError(Exception ex, String title, Application app) {
     *     String msg = bundle.getString("message.error") 
     *             + ex.getLocalizedMessage()
     *             + ex.getClass().getSimpleName()
     *             + "\nSee also the error logs: "
     *             + FileUtils.getDirectory(StorageLocations.LOG_DIR)
     *                     .toString();
     *
     *     JOptionPane.showMessageDialog(
     *             parent,
     *             msg,
     *             bundle.getString("message.error.title")
     *             + title,
     *             JOptionPane.ERROR_MESSAGE); 
     * }
     * </pre>
     *
     * @param parent the component on which this {@code MessageBox} should be
     * centered
     * @param ex The exception that was thrown.
     * @param title The title of the message box
     */
    public static void showError(Component parent, Exception ex, String title) {
        String msg = "The following exception was thrown by the program: "
                + ex.getLocalizedMessage() + " "
                + ex.getClass().getSimpleName() + "\nSee also the error "
                + "logs: " + FileUtils.getDirectory(StorageLocations.LOG_DIR)
                        .toString();

        JOptionPane.showMessageDialog(
                parent,
                msg,
                "Error: " + title,
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Standardized method of displaying a question to the user.
     * <p>
     * This method should be used to request confirmation from the user for any
     * particular action they may have taken. Mostly, this method should be used
     * to confirm closing a window that has unsaved changes.</p>
     * <p>
     * The {@code MessageBox.askQuestion()} method uses the {@link java.util.ResourceBundle
     * ResourceBundle} to retrieve the {@code MessageBox}'s title prefix, and
     * adds to it the passed title for the message box. Therefore, the
     * lead portion of the title is already present by default, so only the
     * message specifics title portion needs to be provided. For example, if the
     * message is telling the user that changes were successfully saved, all
     * that needs to be provided to this message for the title is the value
     * "Save Successful". The `askQuestion()` method is implemented as:</p>
     * <pre>
     * public static int askQuestion(String question, String title, 
     *         boolean cancel) { 
     *     if (cancel) { 
     *         return JOptionPane.showConfirmDialog(
     *                 parent,
     *                 question,
     *                 bundle.getString("message.confirm.title")
     *                         + title,
     *                 JOptionPane.YES_NO_CANCEL_OPTION);
     *     }
     * 
     *     return JOptionPane.showConfirmDialog(
     *             parent, 
     *             question,
     *             bundle.getString("message.confirm.title") + title,
     *             JOptionPane.YES_NO_OPTION); 
     * }
     * </pre>
     *
     * @param parent the component on which this {@code MessageBox} should be
     * centered
     * @param question The question that the user needs to answer.
     * @param title The title of the message box.
     * @param cancel Whether to include a cancel button as an option.
     * @return an integer indicating the option selected by the user. This will
     * be one of:
     * <ul>
     * <li>MessageBox.YES_OPTION</li>
     * <li>MessagBox.NO_OPTION</li>
     * <li>MessageBox.CANCEL_OPTION</li>
     * </ul>
     * if cancel button is included
     *
     */
    public static int askQuestion(Component parent, String question, String title,
            boolean cancel) {
        if (cancel) {
            return JOptionPane.showConfirmDialog(
                    parent,
                    question,
                    "Confirm: " + title,
                    JOptionPane.YES_NO_CANCEL_OPTION);
        }
        return JOptionPane.showConfirmDialog(
                parent,
                question,
                "Confirm: " + title,
                JOptionPane.YES_NO_OPTION);
    }

    /**
     * Standardized method of seeking additional input from the user.
     * <p>
     * This method should be used to prompt the user for a single piece of input
     * that they may have forgotten to provide in another manner. A good use of
     * this method would be to get the value of a required field that the user
     * did not enter on a form.</p>
     * <p>
     * The {@code MessageBox.getInput()} method uses the {@link java.util.ResourceBundle
     * ResourceBundle} to retrieve the {@code MessageBox}'s title prefix, and
     * adds to it the passed title for the message box. Therefore, the lead portion of
     * the title is already present by default, so only the message specifics
     * title portion needs to be provided. For example, if the message is
     * telling the user that changes were successfully saved, all that needs to
     * be provided to this message for the title is the value "Save Successful".
     * The `getInput()` method is implemented as:</p>
     * <pre>
     * public static String getInput(String prompt, String title) { 
     *     return JOptionPane.showInputDialog(
     *             parent,
     *             prompt,
     *             bundle.getString("message.input.title")
     *                     + title), 
     *             JOptionPane.OK_CANCEL_OPTION);
     * }
     * </pre>
     *
     * @param parent the component on which this {@code MessageBox} should be
     * centered
     * @param prompt A description of the information the user needs to provide.
     * @param title The title of the message box.
     * @return The input requested of the user.
     */
    public static String getInput(Component parent, String prompt, String title) {
        return JOptionPane.showInputDialog(
                parent,
                prompt,
                "Provide Input: " + title,
                JOptionPane.PLAIN_MESSAGE);
    }

}
