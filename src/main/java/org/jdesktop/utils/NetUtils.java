/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   PekinSOFT_Utils
 *  Class      :   NetUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 13, 2021 @ 11:29:50 PM
 *  Modified   :   Feb 13, 2021
 *
 *  Purpose:     See class JavaDoc comment.
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 13, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * A static class to allow for the detection of network connections, possibly with 
 * internet access.
 *
 * @author Sean Carrick &lt;PekinSOFT at outlook dot com&gt;
 *
 * @version 0.1.0
 * @since 0.1.0
 */
public class NetUtils {
    
    private NetUtils() {
        // Preventing initialization
    }

    /**
     * An improved version of determining if the local computer is connected to
     * the Internet. This version simply attempts to connect to a website. If
     * the Internet is reachable, it returns {@code true}, otherwise {@code false}
     * is returned.
     * <p>
     * For best results, use this method in conjunction with the {@code canPing}
     * method to be absolutely positive the Internet is reachable.</p>
     *
     * @return {@code true} if the Internet is available and reachable;
     *          {@code false} otherwise.
     *
     * @see #canPing
     */
    public static boolean isNetworkConnectionAvailable() {
        try {
            URL url = new URL("https://gitlab.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            return true;
        } catch (IOException ex) {
            MessageBox.showWarning(null, "Internet connection failed!",
                    "Internet Unavailable");
            return false;
        }
    }

    /**
     * Tests to see whether the web site passed to this method can be pinged. If
     * it can, then the computer is connected to a network with internet access.
     * 
     * @param siteToPing the domain name of the site to attempt to ping
     * @return {@code true} if the ping was successful; {@code false} if not
     */
    public static boolean canPing(String siteToPing) {
        try {
            Process p = Runtime.getRuntime().exec("ping -c 5 " + siteToPing);
            int x = p.waitFor();
            return x == 0;
        } catch (IOException | InterruptedException ex) {
            MessageBox.showError(null, ex, ex.getClass().getSimpleName());
            return false;
        }
    }

}
