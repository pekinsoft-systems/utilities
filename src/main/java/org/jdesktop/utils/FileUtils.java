/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   PekinSOFT_Utils
 *  Class      :   FileUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 17, 2021 @ 1:03:31 PM
 *  Modified   :   Aug 21, 2022
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 17, 2021  Sean Carrick         Initial creation.
 *  Aug 21, 2022  Sean Carrick         Deprecated all of the application directory
 *                                     methods due to having what they provided
 *                                     now be available from the LocalStorage
 *                                     class in the Application API.
 *  Jan 19, 2023  Sean Carrick         Returned the ability to get specific
 *                                     directory locations for particular file
 *                                     types, but updated everything to the NIO
 *                                     classes. This is all part of breaking up
 *                                     the massively overwrought Application API
 *                                     and Maven-izing the individual parts of it.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.lang.System.Logger;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.nio.file.attribute.UserPrincipal;
import java.util.Enumeration;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * The {@code FileUtils} class provide numerous static methods for manipulating
 * physical files.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class FileUtils {

private static final Logger logger = System.getLogger("PS-UTILS-API");
    private static String appId;
    private static String vendorId;

    private static Path appHomeDir = null;

    private FileUtils() {
        /* No instantiation allowed. */ }

    /**
     * Stores the id for the application in which these utilities are being used
     * so that the physical locations on disk will be able to follow the
     * standards as set forth by the various operating systems.
     *
     * @param id the unique identifier for the application
     */
    public static void setAppId(String id) {
        appId = id;
    }

    /**
     * Retrieves the current setting of the application unique identifier being
     * used to determine the proper file locations.
     *
     * @return the unique application identifier currently in use
     */
    public static String getAppId() {
        return appId;
    }

    /**
     * Stores the id for the application vendor for which these utilities are
     * being used, so that the physical locations on disk will be able to follow
     * the standards set forth by the various operating systems.
     *
     * @param id the unique identifier for the application vendor
     */
    public static void setVendorId(String id) {
        vendorId = id;
    }

    /**
     * Retrieves the current setting of the application vendor unique identifier
     * being used to determine the proper file locations.
     *
     * @return the unique application vendor identifier currently in use
     */
    public static String getVendorId() {
        return vendorId;
    }

    /**
     * Retrieves a directory within an application's home directory, or on the
     * local system's file system, depending upon the provided {@link
     * org.jdesktop.utils.StorageLocations location} specified. In order
     * to make sure that the returned directory is in the proper location, the
     * specific application must
     * also be provided to give the starting point for getting the requested
     * directory.
     *
     * Operating systems have specific rules on where applications may and may
     * not store information, such as generated data, configuration files, logs,
     * etc. Therefore, this method makes sure that the directory returned is in
     * the proper place for the operating system on which the `Application` is
     * running, and for the type of directory that is being sought.
     *
     * For example, log files are stored in {@code
     * %SystemRoot%\System32\config\vendorId\} on Microsoft Windows, in {@code
     * ${user.home}/Library/Logs/${application.id}/} on Mac OS X, and in {@code
     * /var/log/${application.id}/} on Linux systems.
     *
     * Configuration files also have their proper places on each OS, such as
     * {@code ${user.home}/Library/Preferences/${application.id}/} on Mac OS X,
     * or {@code /etc/${application.name}/} on Linux.
     *
     * This method does its best to return the appropriate directory for the
     * requested {@code StorageLocation} for the OS on which it is being used.
     * To this end, *some directory* **will be** returned, and physically exist
     * on disk, regardless. If the current user does not have read or write
     * access to a system-wide location, such as {@code
     * /var/log/} on Linux, a directory within the user's home directory will
     * be provided as the return value of this method.
     *
     * In the event that this method is passed
     * {@code StorageLocations.FILE_SYSTEM}, a directory of the current user's
     * home directory is returned.
     *
     * It is guaranteed that the directory {@link java.io.File File} returned
     * from this method physically exists on disk.
     *
     * @param location a {@code StorageLocations} enum constant for the
     * directory sought
     * @return a directory for the `Application`, in the location specified
     * @throws IllegalArgumentException if either {@code location} or
     * {@code app} is null
     */
    public static Path getDirectory(StorageLocations location) {
        if (location == null) {
            throw new IllegalArgumentException("null location");
        }
        try {
            switch (location) {
                case CONFIG_DIR: return getConfigDir();
                case DATA_DIR: return getDataDir();
                case ERROR_DIR: return getErrorDir();
                case LOG_DIR: return getLogDir();
                case OPTIONS_DIR: return getOptionalDir();
                case PLUGINS_DIR: return getPluginsDir();
                case SYSTEM_DIR: return getSystemDir();
                case VARIANTS_DIR: return getVariantsDir();
                default: return getDirectory();
            }
        } catch (IOException ex) {
            String message = String.format("Unable to get directory type "
                    + "\"%s\": %s", location, ex.getMessage());
            logger.log(Logger.Level.ERROR, message, ex);
            return null;
        }
    }

    /**
     * Retrieves a file from the specified {@link org.jdesktop.utils.StorageLocations
     * StorageLocations}, with the given {@code fileName}
     *
     * @param location the {@code StorageLocations} enum constant for where the
     * file should be located
     * @param fileName the name of the file to get
     * @return the specified {@code StorageLocations}/{@code fileName}
     * @throws IllegalArgumentException if either parameter is null or if the
     * {@code fileName} parameter is empty
     */
    public static File getFile(StorageLocations location, String fileName) {
        if (location == null) {
            throw new IllegalArgumentException("null StorageLocation");
        }
        if (fileName == null) {
            throw new IllegalArgumentException("null fileName");
        } else if (fileName.isEmpty()) {
            throw new IllegalArgumentException("empty fileName");
        }
        
        Path file = Paths.get(getDirectory(location).toString(), fileName);

        return file.toFile();
    }

    /**
     * Retrieves a {@link java.io.Writer BufferedWriter} object for data output.
     *
     * @param location the {@code StorageLocations} in which the file should be
     * created
     * @param fileName the name of the file to be created
     * @return a {@code BufferedWriter} object for writing data to the file
     * @throws IOException if any input/output exceptions occur while getting
     * the {@code BufferedWriter} object
     * @throws IllegalArgumentException if either parameter is null or the
     * {@code fileName} parameter is empty
     */
    public static Writer getBufferedWriter(StorageLocations location, String fileName)
            throws IOException {
        if (location == null) {
            throw new IllegalArgumentException("null location");
        }
        if (fileName == null || fileName.trim().isEmpty()) {
            throw new IllegalArgumentException("null or empty fileName");
        }
        Path file = Paths.get(getDirectory(location).toString(), fileName);
        return Files.newBufferedWriter(file);
    }
    
    /**
     * Retrieves a {@link java.io.BufferedReader BufferedReader} for the given
     * file name.
     * 
     * @param location the {@link org.jdesktop.utils.StorageLocations
     * StorageLocation} from which to retrieve the reader
     * @param fileName the name of the file against which to get a reader
     * @return a {@code BufferedReader} for the specified file
     * @throws IOException in the event of any input/output exceptions
     * @throws IllegalArgumentException if either parameter is null or the
     * {@code fileName} parameter is empty
     */
    public static Reader getFileReader(StorageLocations location, String fileName) 
            throws IOException {
        if (location == null) {
            throw new IllegalArgumentException("null location");
        }
        if (fileName == null || fileName.trim().isEmpty()) {
            throw new IllegalArgumentException("null or empty fileName");
        }
        Path file = Paths.get(getDirectory(location).toString(), fileName);
        return Files.newBufferedReader(file);
    }

    private static OSId getOSId() {
        String osName = System.getProperty("os.name").toLowerCase();
        OSId osId = OSId.UNIX;

        if (osName.contains("mac os x")) {
            osId = OSId.OSX;
        } else if (osName.contains("windows")) {
            osId = OSId.WINDOWS;
        }

        return osId;
    }

    private static enum OSId {
        WINDOWS, OSX, UNIX
    }

    /**
     * Delete the specified file from disk.
     *
     * @param toDelete the file to be deleted.
     * @return {@code true} if the file exists and is successfully deleted;
     * {@code false} on any failure
     */
    public static boolean deleteFile(final Path toDelete) {
        try {
            Files.delete(toDelete);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Copies the file {@code toCopy} to the destination file {@code destFile}.
     *
     * @param source the file to copy
     * @param destination the new location of the copied file
     * @return `true` upon success; `false` upon failure
     */
    public static boolean copyFile(final File source, final File destination) {
        try {
            return FileUtils.copyStream(new FileInputStream(source),
                    new FileOutputStream(destination));
        } catch (final FileNotFoundException e) {
            String msg = String.format("%s: Unable to copy source file, \"%s\", " // NOI18N
                    + "to the destination file, \"%s\".", e, source, destination); // NOI18N
            System.err.println(msg);
        }

        return false;
    }

    /**
     * Copies all files in the {@code sourceDir} to the destination {@code
     * destDir}.
     *
     * @param sourceDir the folder that contains the files to copy
     * @param destDir the destination folder for the copied files
     * @return `true` upon success; `false` upon failure
     */
    private static boolean copyFilesRecursively(final File sourceDir,
            final File destDir) {
        assert destDir.isDirectory();

        if (!sourceDir.isDirectory()) {
            return FileUtils.copyFile(sourceDir, new File(destDir, sourceDir.getName()));
        } else {
            final File newDestDir = new File(destDir, sourceDir.getName());

            if (!newDestDir.exists() && !newDestDir.mkdir()) {
                return false;
            }

            for (final File child : sourceDir.listFiles()) {
                if (!FileUtils.copyFilesRecursively(child, newDestDir)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Recursively copies all resources contained with a the specified Java
     * ARchive (JAR) file. This provides a means of "flattening" a JAR file's
     * contents into a directory structure on disk.
     *
     * @param destDir the directory into which the JAR contents should be
     * written
     * @param jarConnection the connection to the JAR file
     * @return {@code true} upon success; {@code false} on failure
     * @throws IOException if any input/output errors occur
     */
    public static boolean copyJarResourcesRecursively(final File destDir,
            final JarURLConnection jarConnection) throws IOException {
        final JarFile jarFile = jarConnection.getJarFile();

        for (final Enumeration<JarEntry> e = jarFile.entries();
                e.hasMoreElements();) {
            final JarEntry entry = e.nextElement();

            if (entry.getName().startsWith(jarConnection.getEntryName())) {
                final String filename = StringUtils.removeStart(entry.getName(),
                        jarConnection.getEntryName());

                final File f = new File(destDir, filename);

                if (!entry.isDirectory()) {
                    final InputStream entryInputStream = jarFile.getInputStream(entry);

                    if (!FileUtils.copyStream(entryInputStream, f)) {
                        return false;
                    }

                    entryInputStream.close();
                } else {
                    if (!FileUtils.ensureDirectoryExists(f)) {
                        throw new IOException("Could not create directory: "
                                + f.getAbsolutePath());
                    }
                }
            }
        }

        return true;
    }

    public static boolean copyResourcesRecursively(final URL source,
            final File destination) {
        try {
            final URLConnection urlConnection = source.openConnection();

            if (urlConnection instanceof JarURLConnection) {
                return FileUtils.copyJarResourcesRecursively(destination,
                        (JarURLConnection) urlConnection);
            } else {
                return FileUtils.copyFilesRecursively(new File(
                        source.getPath()),
                        destination);
            }
        } catch (final IOException e) {
            String msg = String.format("%s: Unable to copy source URL, \"%s\", " // NOI18N
                    + "to the destination file, \"%s\".", e, source, destination); // NOI18N
            System.err.println(msg);
        }

        return false;
    }

    private static boolean copyStream(final InputStream source, final File destination) {
        try {
            return FileUtils.copyStream(source, new FileOutputStream(destination));
        } catch (final FileNotFoundException e) {
            String msg = String.format("%s: Unable to copy source file, \"%s\", " // NOI18N
                    + "to the destination file, \"%s\".", e, source, destination); // NOI18N
            System.err.println(msg);
        }

        return false;
    }

    private static boolean copyStream(final InputStream source,
            final OutputStream destination) {
        try {
            final byte[] buf = new byte[1024];

            int len = 0;
            while ((len = source.read(buf)) > 0) {
                destination.write(buf);
            }

            source.close();
            destination.close();
            return true;
        } catch (final IOException e) {
            String msg = String.format("%s: Unable to copy source file, \"%s\", " // NOI18N
                    + "to the destination file, \"%s\".", e, source, destination); // NOI18N
            System.err.println(msg);
        }

        return false;
    }

    /**
     * Provides a means of determining if the given directory exists, and is not
     * a regular file. If {@code f} is a regular file, `false` is returned. If
     * {@code f} does not exist, the directory is created. Whether or not the
     * directory is created, `true` is returned if the directory exists.
     *
     * @param f the file to verify, or create
     * @return `false` if the file is a regular file; `true` if the directory
     * did not exist, but was created, or already exists; `false` otherwise
     */
    public static boolean ensureDirectoryExists(final File f) {
        if (!f.isDirectory()) {
            return false;
        } else if (f.isDirectory()) {
            return true;
        } else if (!f.exists()) {
            return f.mkdir();
        }

        return false;
    }

    private static Path getDirectory() throws IOException {
        if (appHomeDir != null) {
            return appHomeDir;
        }

        OSId osId = getOSId();
        if (osId == null) {
            String path = "." + getAppId() + File.separator;
            appHomeDir = Paths.get(getUserHome(), path);
        } else {
            switch (osId) {
                case OSX:
                    // getUserHome()/Library/Application Support/getAppId()
                    appHomeDir = Paths.get(
                            getUserHome(),
                            "Library",
                            "Application Support",
                            getAppId()
                    );
                    break;
                case WINDOWS:
                    // getUserHome()\\%APPDATADIR%\\getVendorId()\\getAppId()
                    //+ OR
                    //+ getUserHome()\\Application Data\\getVendorId()\\getAppId()
                    Path appDataDir = null;
                    try {
                        String appDataEV = System.getenv("APPDATA");
                        if (((appDataEV != null) && appDataEV.length() > 0)) {
                            appDataDir = Paths.get(appDataEV);
                        }
                    } catch (SecurityException ignore) {
                    }

                    if (appDataDir != null && Files.isDirectory(appDataDir)) {
                        appHomeDir = Paths.get(
                                appDataDir.toString(),
                                getVendorId(),
                                getAppId()
                        );
                    } else {
                        appHomeDir = Paths.get(
                                getUserHome(),
                                "Application Data",
                                getVendorId(),
                                getAppId()
                        );
                    }
                    break;
                default:
                    // getUserHome()/.getAppId()/
                    appHomeDir = Paths.get(getUserHome(), "." + getAppId());
                    break;
            }
        }
        
        // Verify that the directory exists and the path is writable.
        if (!Files.exists(appHomeDir)) {
            if (canWrite(appHomeDir)) Files.createDirectories(appHomeDir);
        }

        return appHomeDir;
    }

    private static Path getConfigDir() throws IOException {
        Path config = null;
        
        switch (getOSId()) {
            case OSX:
                // getUserHome()/Library/Preferences/getAppId()
                config = Paths.get(
                        getUserHome(), 
                        "Library", 
                        "Preferences",
                        getAppId()
                );
                break;
            case WINDOWS:
                // APPDATA\\getVendorId()\\getAppId()\\config
                config = Paths.get(getDirectory().toString(), "config");
                break;
            default:
                // getUserHome()/.getAppId()/etc
                config = Paths.get(getDirectory().toString(), "etc");
                break;
        }
        
        // Verify that the directory exists and the path is writable.
        if (!Files.exists(config)) {
            if (canWrite(config)) Files.createDirectories(config);
        }
        
        return config;
    }

    private static Path getDataDir() throws IOException {
        Path data = Paths.get(getDirectory().toString(), "data");
        
        if (!Files.exists(data)) {
            if (canWrite(data)) Files.createDirectories(data);
        }
        
        return data;
    }

    private static Path getErrorDir() throws IOException {
        Path errors = Paths.get(getLogDir().toString(), "err");
        
        if (!Files.exists(errors)) {
            if (canWrite(errors)) Files.createDirectories(errors);
        }
        return errors;
    }

    private static Path getLogDir() throws IOException {
        Path logs = null;
        
        switch (getOSId()) {
            case WINDOWS:
                // %SystemRoot%\\System32\\config\\getAppId()\\
                logs = Paths.get(
                        System.getenv("%SystemRoot%"),
                        "System32",
                        "config",
                        getAppId()
                );
                
                if (!canWrite(logs)) {
                    // Alternate log location:
                    //+ getVariantsDir()\\var\\log\\
                    logs = Paths.get(getVariantsDir().toString(), "log");
                }
                break;
            case OSX:
                // getUserHome()/Library/Logs/getAppId()/
                logs = Paths.get(
                        getUserHome(),
                        "Library",
                        "Logs",
                        getAppId()
                );
            default:
                // /var/logs/getAppId()/
                logs = Paths.get("/", "var", "log", getAppId());
                
                if (!canWrite(logs)) {
                    // Alternate log location:
                    //+ getVariantsDir()/log/
                    logs = Paths.get(
                            getVariantsDir().toString(),
                            "log"
                    );
                }
                break;
        }
        
        // Make sure the directory exists and is writable.
        if (!Files.exists(logs)) {
            if (canWrite(logs)) Files.createDirectories(logs);
        }
        
        return logs;
    }

    private static Path getOptionalDir() throws IOException {
        Path options = Paths.get(
                getDirectory().toString(),
                "opt"
        );
        
        if (!Files.exists(options)) {
            if (canWrite(options)) Files.createDirectories(options);
        }
        
        return options;
    }

    private static Path getPluginsDir() throws IOException {
        // TODO: Figure out the best way to accomplish this, or get rid of it.
        throw new UnsupportedOperationException("getPluginsDir has not been figured out yet.");
    }

    private static Path getSystemDir() throws IOException {
        Path system = Paths.get(
                getDirectory().toString(),
                "sys"
        );
        
        if (!Files.exists(system)) {
            if (canWrite(system)) Files.createDirectories(system);
        }
        
        return system;
    }

    private static Path getVariantsDir() throws IOException {
        Path variants = Path.of(
                getDirectory().toString(),
                "var"
        );
        
        if (!Files.exists(variants)) {
            if (canWrite(variants)) Files.createDirectories(variants);
        }
        
        return variants;
    }

    private static String getUserHome() {
        return System.getProperty("user.home");
    }
    
    private static boolean canWrite(Path path) throws IOException {
        Path parent = null;
        if (!Files.isDirectory(path)) {
            parent = path.getParent();
        }
        
        try {
            UserPrincipal owner = null;
            if (parent != null) {
                owner = Files.getOwner(parent);
            } else {
                owner = Files.getOwner(path);
            }
            if (System.getProperty("user.name").equals(owner.getName())) {
                return true;
            }
                
            switch (getOSId()) {
                case WINDOWS:
                    return !((Boolean) Files.getAttribute(path, "dos:readonly"));
                default:
                    PosixFileAttributeView view = Files.getFileAttributeView(
                            parent, PosixFileAttributeView.class);
                    PosixFileAttributes attribs = view.readAttributes();
                    Set<PosixFilePermission> perms = attribs.permissions();
                    String rwxrwxrwx = PosixFilePermissions.toString(perms);
                    if (owner.getName().equals(System.getProperty("user.name"))) {
                        return rwxrwxrwx.charAt(1) == 'w';
                    } else {
                        return rwxrwxrwx.charAt(7) == 'w';
                    }
            }
        } catch (IOException ex) {
            if (ex.getMessage().contains("var")) {
                // We are probably looking at the 'var' directory on *nix or Mac,
                //+ so let's see if the user can write to the parent.
                canWrite(path.getParent());
            } else {
                // Otherwise, just rethrow the exception.
                throw ex;
            }
            return false; // Better safe than sorry.
        }
    }

}
