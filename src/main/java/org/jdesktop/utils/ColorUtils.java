/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   PekinSOFT_Utils
 *  Class      :   ColorUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 2, 2022
 *  Modified   :   Dec 2, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 2, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.utils;

import java.awt.Color;

/**
 * The {@code ColorUtils} class is derived from an example I found online for
 * finding the difference between colors. The author is named Martin Mustin and
 * the code is released under the GNU General Public License Version 3.
 * <p>
 * These methods are useful in custom {@link javax.swing.table.TableModel
 * TableModels} when altering background colors of rows, to make sure that the
 * UI foreground text is visible and not difficult to see.</p>
 * <p>
 * For example, if the background color of a table row is set to:</p>
 * <pre>
 * Color(0.940f, 0.940f, 1.0f)  // Extremely light blue
 * </pre><p>
 * If the UI Manager has the foreground text in {@code TextField}s as a very
 * light color, due to the overall theme being a dark one (such as FlatLaf Dark),
 * then the text in this row would be difficult to see, at best, and not visible
 * at all at worst.</p>
 * <p>
 * Using these methods, one could make sure that a very light foreground color
 * is not used for the text in a table row that is using a very light background.
 * So, by testing the UI Manager's foreground color for a {@code TextField}, one
 * could set the foreground of their table row accordingly, by using the UI
 * Manager's foreground color if it is a dark color, or an alternative color,
 * such as {@code Color.black} if it is not.</p>
 *
 * @author Martin Mustin (copyrighted 2008-2011)
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class ColorUtils {
    
    private ColorUtils () { /* Not instantiable. */}
    
    /**
     * Check if a color is more dark than light. Useful if an entity of this 
     * color is labeled: use white or lighter color on a "dark" color and a black
     * or darker color on a "light" color.
     * 
     * @param r red value of the color to check
     * @param g green value of the color to check
     * @param b blue value of the color to check
     * @return {@code true} if the specified color is a "dark" color; {@code false}
     * if it is a "light" color
     */
    public static boolean isDarkColor(double r, double g, double b) {
        double white = colorDistance(r, g, b, 1.0, 1.0, 1.0);
        double black = colorDistance(r, g, b, 0.0, 0.0, 0.0);
        return black < white;
    }
    
    /**
     * Check if the specified color is more dark than light. Useful if an entity
     * of this color is labeled: use white or lighter color on a "dark" color
     * and black or darker color on a "light" color.
     * 
     * @param color2Check the {@link java.awt.Color Color} to check
     * @return {@code true} if the specified color is a "dark" color; {@code false}
     * if it is a "light" color
     */
    public static boolean isDarkColor(Color color2Check) {
        float r = color2Check.getRed() / 255.0f;
        float g = color2Check.getGreen() / 255.0f;
        float b = color2Check.getBlue() / 255.0f;
        return isDarkColor(r, g, b);
    }
    
    /**
     * Calculates the "distance" between two colors. 
     * 
     * @param color1 the first {@link java.awt.Color Color} to compare
     * @param color2 the second {@code Color} to compare
     * @return the "distance" between the two specified {@code Color}s
     */
    public static double colorDistance(Color color1, Color color2) {
        float[] rgb1 = new float[3];
        float[] rgb2 = new float[3];
        color1.getColorComponents(rgb1);
        color2.getColorComponents(rgb2);
        return colorDistance(rgb1[0], rgb1[1], rgb1[2], rgb2[0], rgb2[1], rgb2[2]);
    }
    
    /**
     * Calculates the "distance" between the two colors.
     * 
     * @param color1 the first color to compare
     * @param color2 the second color to compare
     * @return the "distance" between the two specified colors
     */
    public static double colorDistance(double[] color1, double[] color2) {
        return colorDistance(color1[0], color1[1], color1[2], color2[0],
                color2[1], color2[2]);
    }
    
    /**
     * Calculates the "distance" between the two colors. The RGB entries are 
     * taken to be coordinates in a 3D space (0.0 - 1.0], and this method 
     * returns the distance between the coordinates for the first and second
     * colors.
     * 
     * @param r1 red value of the first color
     * @param g1 green value of the first color
     * @param b1 blue value of the first color
     * @param r2 red value of the second color
     * @param g2 green value of the second color
     * @param b2 blue value of the second color
     * @return the "distance" between the two colors
     */
    public static double colorDistance(double r1, double g1, double b1, double r2,
            double g2, double b2) {
        double r = r2 - r1;
        double g = g2 - g1;
        double b = b2 - b1;
        return Math.sqrt(r * r + g * g + b * b);
    }

}
