/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   PekinSOFT_Utils
 *  Class      :   StringUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 11, 2022
 *  Modified   :   Dec 11, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 11, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.utils;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class StringUtils {
    
    private StringUtils () { /* No instantiation allowed. */ }
    
    /**
     * Abbreviates (or more accurately, <em>truncates</em>) a string, and ends it
     * with an ellipsis ("&hellip;").
     * <p>
     * For example, if the string "Now is the time for all good men" is supplied
     * as the {@code source}, with a specified {@code width} of nineteen (19),
     * the value returned would be "Now is the time ..."</p>
     * <p>
     * The rules applied are as follows:</p>
     * <ul>
     * <li>If the length of {@code source} is less than {@code width}; return
     * {@code source} unchanged.</li>
     * <li>If the length of {@code source} is greater than {@code width}; 
     * truncate source to {@code width - 3} and append the ellipsis.</li>
     * <li>If {@code width} is less than five (5), throw an 
     * IllegalArgumentException.</li>
     * <li>If {@code source} is null, throw an IllegalArgumentException.</li>
     * </ul>
     * 
     * @param source the string to be abbreviated/truncated
     * @param width the width at which truncation should happen
     * @return the abbreviated/truncated string, with an ellipse appended
     * @throws IllegalArgumentException if the {@code source} string is {@code 
     * null} or if {@code width} is less than five (5)
     */
    public static String abbreviate(String source, int width) {
        if (source == null) {
            throw new IllegalArgumentException("null source string");
        }
        if (width < 5) {
            throw new IllegalArgumentException("width must be greater than or "
                    + "equal to five (5)");
        }
        if (source.length() < width) {
            return source;
        }
        
        return source.substring(0, width - 3) + "...";
    }
    
    /**
     * Deletes all whitespace characters from the specified {@code source}
     * string.
     * <p>
     * For example, if the string value "I'm just a poor boy, nobody loves me" is
     * specified as {@code source}, then the value returned would be 
     * "I'mjustapoorboy,nobodylovesme".</p>
     * 
     * @param source the string from which whitespace characters are to be
     * removed
     * @return the {@code source} string with no whitespace characters in it
     * @throws IllegalArgumentException if the {@code source} string is null
     */
    public static String deleteWhitespace(String source) {
        if (source == null) {
            throw new IllegalArgumentException("null source string");
        }
        
        char[] chars = source.toCharArray();
        source = "";
        
        for (char c : chars) {
            if (!Character.isWhitespace(c)) {
                source += c;
            }
        }
        
        return source;
    }

    /**
     * Removes a substring only if it is at the beginning of a source string,
     * otherwise returns the source string. A null source string will return
     * null. An empty ("") source string will return the empty string. A
     * null search string will return the source string.
     *
     * @param source the string from which the substring should be removed.
     * @param remove the substring to remove from the source string.
     * @return java.lang.String the source string with the substring removed,
     * except as provided for above.
     */
    public static String removeStart(String source, String remove) {
        if (source == null) {
            return null;
        } else if (source.isEmpty()) {
            return source;
        }
        if (remove == null) {
            return source;
        }

        String holder = "";

        if (source.startsWith(remove)) {
            holder = source.substring(remove.length());
        }

        return holder;
    }

    /**
     * Removes the beginning substring from the {@code source} string. 
     * <p>
     * If {@code source} has the value "Tonight is THE night", and {@code remove}
     * has the value "Tonight is", then the returned string will be "THE night".
     * </p><p>
     * If the {@code source} string does not start with the {@code remove} string,
     * then the {@code source} string is returned unaltered.
     * 
     * @param source the unaltered source string
     * @param remove the value to remove from the source string
     * @return the source string with the specified part removed from the 
     * beginning of the string
     */
    public static String removeStartIgnoreCase(String source, String remove) {
        if (source == null) {
            return null;
        } else if (source.isEmpty()) {
            return source;
        }
        if (remove == null) {
            return source;
        }

        String holder = "";

        if (source.toLowerCase().startsWith(remove.toLowerCase())) {
            holder = source.substring(remove.length());
        } else {
            holder = source;
        }

        return holder;
    }

    /**
     * Removes a substring only if it is at the end of a source string,
     * otherwise returns the source string.
     * <p>
     * A null source string will return null. An empty ("") source string
     * will return the empty string. A null search string will return the
     * source string.</p>
     * <p>
     * If the source string does not end with the search string, then the source
     * string will be returned unaltered.</p>
     *
     * @param source the source String to search, may be null
     * @param remove the String to search for and remove, may be null
     * @return the substring with the string removed, if found, null if null
     * String input
     */
    public static String removeEnd(String source, String remove) {
        if (source == null) {
            return null;
        }
        if (remove == null) {
            return source;
        }
        if (source.isEmpty()) {
            return source;
        }

        if (source.endsWith(remove)) {
            return source.substring(0, source.indexOf(remove));
        } else {
            return source;
        }
    }
    
    /**
     * Pads the provided string to the left side of the field, filling the
     * remaining field width with spaces. If the specified string is longer than
     * the field, then the string is truncated to the field width.
     * <p>
     * For example, if the string is "Michael" and the {@code fieldWidth} is
     * ten (10), then the returned string would be "Michael   ". If the string
     * is "Johnson" and the {@code fieldWidth} is five (5), the returned string
     * would be "Jo...".</p>
     * <p>
     * If the specified {@code fieldWidth} is less than five, then it will be set
     * to five and no exception will be thrown.</p>
     * 
     * @param toPad the string to be padded for a left-aligned field
     * @param fieldWidth the width of the left-aligned field
     * @return a padded or truncated string that will fit in the specified
     * {@code fieldWidth}
     * @throws IllegalArgumentException if the specified `toPad` string is null
     */
    public static String padLeft(String toPad, int fieldWidth) {
        if (toPad == null) {
            throw new IllegalArgumentException("null toPad string");
        }
        if (fieldWidth < 5) {
            fieldWidth = 5;
        }
        
        if (toPad.isEmpty()) {
            return repeat(" ", fieldWidth);
        } else if (toPad.length() > fieldWidth) {
            return abbreviate(toPad, fieldWidth);
        } else {
            int difference = fieldWidth - toPad.length();
            return toPad + repeat(" ", difference);
        }
    }
    
    /**
     * Pads the provided string to the right side of the field, filling the 
     * beginning of the field with spaces. If the specified string is longer than
     * the width of the field, then the string is truncated to the field width.
     * <p>
     * For example, if the string is "Michael" and the {@code fieldWidth} is
     * ten (10), then the returned string would be "   Michael". If the string
     * is "Johnson" and the {@code fieldWidth} is five (5), the returned string
     * would be "Jo...".</p>
     * <p>
     * If the specified {@code fieldWidth} is less than five, it will be set to
     * five and no exception will be thrown.</p>
     * 
     * @param toPad the string to be padded to the right-aligned field
     * @param fieldWidth the width of the right-aligned field
     * @return a padded or truncated string that will fit in the specified
     * {@code fieldWidth}, right-aligned
     * @throws IllegalArgumentException if {@code toPad} is null
     */
    public static String padRight(String toPad, int fieldWidth) {
        if (toPad == null) {
            throw new IllegalArgumentException("null toPad string");
        }
        if (fieldWidth < 5) {
            fieldWidth = 5;
        }
        
        String padded;
        
        if (toPad.isEmpty()) {
            padded = repeat(" ", fieldWidth);
        } else if (toPad.length() > fieldWidth) {
            padded = abbreviate(toPad, fieldWidth);
        } else {
            int difference = fieldWidth - toPad.length();
            padded = repeat(" ", difference) + toPad;
        }
        
        // Debugging code for tests...
        System.out.println("Returning from padRight:\n");
        System.out.println(padded);
        
        return padded;
    }
    
    /**
     * Repeats the specified string, {@code toRepeat}, the specified number of
     * {@code times}.
     * <p>
     * If {@code toRepeat} is null, it is set to a single space. If
     * {@code times} is less than one (1), an empty string is returned. Otherwise,
     * {@code toRepeat} is returned repeated {@code times} times.</p>
     * <p>
     * If the specified {@code toRepeat} string is null, it will be set to an
     * empty space character. If {@code times} is less than one, it will cause
     * an empty string to be returned. In either case, no exception will be
     * thrown.</p>
     * 
     * @param toRepeat the string to be repeated
     * @param times the number of times to repeat the string
     * @return the string repeated the requested number of times
     */
    public static String repeat(String toRepeat, int times) {
        if (toRepeat == null) {
            toRepeat = " ";
        }
        if (times < 1) {
            return "";
        }
        
        StringBuilder sb = new StringBuilder();
        for (int x = 0; x < times; x++) {
            sb.append(toRepeat);
        }
        
        return sb.toString();
    }
    /**
     * This method will take the {@code source} string and wrap it at the specified
     * {@code width}.
     * <p>
     * When performing the wrapping of the {@code source} string, this method first
     * checks to see if there are any newline characters included within it. If
     * there are, the {@code source} will first be broken into parts at the newline
     * characters. Once that is done, the elements of the created array will be
     * split on the space characters. Once all of this is complete, the string
     * will be rebuilt allowing for wrapping to take place at the specified
     * {@code width}, without breaking within a single word.</p>
     * <p>
     * Furthermore, this method takes into account any hyphens contained within
     * the {@code source} string. If the built up line is getting close to the {@code width}
     * of the requested string and the next word will go beyond the specified
     * {@code width}, this method will check for a hyphen within the next word. If a
     * hyphen exists, the word will be temporarily broken on the hyphen to see
     * if that portion of the word will fit on the current line. If it will, it
     * will be added to the current line of text. If not, then the entire
     * hyphenated word will be placed at the beginning of the next line of text.
     * </p>
     * <dl><dt><strong><em>Note Regarding Additional Splits</em></strong></dt>
     * <dd>When {@code StringUtils.wrap()} is used for prettifying log output, it is
     * best to call the specialized method {@code StringUtils.wrapLogMessage()}. This
     * overloaded version of {@code wrap} replaces comma characters with a newline
     * and tab character. This is due to using {@code String.format()} to create the
     * log messages to log the state of {@code Object}s during debugging. The result
     * from {@code toString} typically involves all properties of an {@code Object} being
     * strung together separated by only the comma, and without spaces. This
     * causes some information to not be wrapped properly, which allows for very
     * long lines, even in the formatted output.</dd></dl>
     *
     * @param source the source string to be wrapped
     * @param width the width at which the {@code source} string should be wrapped
     * @return the {@code source} string, properly wrapped at the specified {@code width}
     * @throws IllegalArgumentException if {@code source} is null, empty, or blank,
     * or if {@code width} is less than or equal to zero
     * 
     * @see #wrapLogMessage(java.lang.String, int) 
     */
    public static String wrap(String source, int width) {
        if (source == null) {
            throw new IllegalArgumentException("null source");
        }
        if (source.isEmpty()) {
            throw new IllegalArgumentException("empty source");
        }
        if (width <= 0) {
            throw new IllegalArgumentException("width <= 0");
        }

        StringBuilder sb = new StringBuilder();

        // First, split on newline characters. If there are none the lines array
        //+ will only have a single element, so the loop will only run once.
        String[] lines = source.split("\n");
        String holder = "";

        for (String line : lines) {
            if (line.length() <= width) {
                sb.append(line).append("\n");
            } else {
                // Split the line into its individual words.
                String[] words = line.split(" ");

                for (String word : words) {
                    // Check if the holder string is less than the desired width.
                    if (holder.length() <= width) {
                        // Check to see if the next word and space will go beyond
                        //+ the desired width.
                        if ((holder.length() + word.length() + 1) <= width) {
                            // If not, add the space and the word to the holder.
                            if (!holder.endsWith(".")) {
                                holder += word + " ";
                            } else {
                                holder += " " + word + " ";
                            }
                        } else {
                            // If so, check to see if the next word has a hyphen.
                            if (word.contains("-")) {
                                // If so, split the word on the hyphen.
                                String[] hyphenated = word.split("-");

                                // Check if hyphenated only has a length of two.
                                if (hyphenated.length == 2) {
                                    // Check if the first element will fit within
                                    //+ the desired width.
                                    if ((holder.length() + hyphenated[0].length()
                                            + 1) <= width) {
                                        // If so, add the first element.
                                        holder += hyphenated[0];

                                        // Append holder to the StringBuilder, 
                                        //+ while adding back the hypen with
                                        //+ a newline character at the end.
                                        sb.append(holder.trim()).append("-");
                                        sb.append("\n");

                                        // Reset holder for the next line.
                                        holder = hyphenated[1] + " ";
                                    }
                                } else if (hyphenated.length > 2) {
                                    // If multiple hyphens, add the first element.
                                    holder += hyphenated[0];

                                    // Append holder to the StringBuilder, 
                                    //+ while adding back the hypen with
                                    //+ a newline character at the end.
                                    sb.append(holder.trim()).append("-").append("\n");

                                    // Add the remaining hyphenated parts to
                                    //+ holder.
                                    for (int x = 1; x < hyphenated.length; x++) {
                                        if (x > 1) {
                                            // Reset holder for the next line.
                                            holder = "";

                                            // Add back the hyphen and the next
                                            //+ part.
                                            holder += "-" + hyphenated[x];
                                        } else {
                                            // Simply add the hyphenated part.
                                            holder += hyphenated[x];
                                        }
                                    }
                                }
                            } else {
                                // The word is not hyphenated, so append holder to
                                //+ the StringBuilder, with a newline character.
                                sb.append(holder.trim()).append("\n");

                                // Reset holder and add the current word to it.
                                holder = word + " ";
                            }
                        }
                    }
                }
            }
        }

        // Once the end of the lines are reached, we need to add whatever text
        //+ is remaining in holder to our StringBuilder.
        sb.append(holder.trim());

        // Debugging code for tests...
        System.out.println("Returning from wrap:\n");
        System.out.println(sb.toString());
        
        String ret = sb.toString().trim();

        return (ret == null || ret.isEmpty()) ? null : ret;
    }

    /**
     * This method will take the {@code source} string and wrap it at the specified
     * {@code width}.
     * <p>
     * <em>This is a specialized method for use with log messages</em>.</p>
     * <p>
     * The specialized use of this method is specifically for formatting log
     * message output. The first thing this method does is replace all commas in
     * the {@code source} with a {@code \n\t} character sequence. This is so that the output
     * from {@code Object.toString()} will also be formatted by splitting it into 
     * separate lines. The common output from the {@code toString} method of any Java
     * {@code Object} is to concatenate all of the object's {@code propertyName=value}s and
     * separate each property/value pair by only a comma, without a space. This
     * method, therefore, replaces the commas with the newline/tab sequence to
     * prevent an object's {@code toString} output from being extremely long and not
     * able to be wrapped properly by the {@code StringUtils.wrap()} method.</p>
     * <p>
     * The other thing that is done by this method is to replace all 
     * semicolon/space character combinations with a newline character (`\n`).
     * Typically when writing log messages, developers will split values they
     * are logging by using a semicolon/space combination. Therefore, this method
     * also splits the {@code source} text on that character combination to better 
     * format the log messages.</p>
     * <p>
     * The body of this method is simply:</p>
     * <pre>
     * public static String wrapLogMessage(String source, int width) {
     *     return wrap(source.replace(",", "\n\t").replace("; ", "\n"), width);
     * }
     * </pre><p>
     * Therefore, the {@code wrap} method is still used and this method really only
     * prepares the {@code source} string for proper wrapping by the {@code wrap} method.
     * </p>
     * 
     * @param source the source string with which to work
     * @param width the width at which the log message should be wrapped
     * @return the {@code source} string, properly wrapped for log message output, to
     * the specified {@code width}
     * @throws IllegalArgumentException if {@code source} is null, empty, or blank,
     * or if {@code width} is less than or equal to zero
     * 
     * @see #wrap(java.lang.String, int) 
     */
    public static String wrapLogMessage(String source, int width) {
        if (source == null || source.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty source");
        }
        if (width <= 0) {
            throw new IllegalArgumentException("width less than or equal to zero");
        }
        
        String pathSplitter = System.getProperty("path.separator");
        String wrapped = wrap(source.replace(",", "\n\t").replace(pathSplitter, "\n"), width);
        
        // Debugging code for tests...
        System.out.println("Returning from wrapLogMessage:\n");
        System.out.println(wrapped);
        
        return wrapped;
    }
    
    /**
     * Inserts a tab leader with the specified character as the leader.
     * <p>
     * A tab leader is such that a character is inserted between two words or
     * phrases, such as:</p>
     * <pre>
     * OS.....................Microsoft Windows
     * Version...............................11
     * </pre><p>
     * When using a tab leader, the right word or phrase is right-aligned with
     * all words and phrases below it and the leader character appears to the
     * left of the word or phrase. The leader character starts on the left at
     * the right edge of the left word or phrase.</p>
     *
     * @param leftWord the word or phrase that is on the left side of the tab
     * @param rightWord the word or phrase that is on the right side of the tab
     * @param rightMargin the farthest right the text should be placed
     * @param leader the character to repeat in the intervening space between
     * the {@code rightWord} and {@code leftWord}. This character needs to be one of
     * underscore (_), period (.), dash (-), or space ( )
     * @return a string formatted with the {@code leftWord} separated from the
     * {@code rightWord} by a repeated {@code leader} character and the {@code rightWord}s
     * right-aligned
     * @throws IllegalArgumentException if {@code leftWord}, or {@code rightWord} are
     * null, blank, or empty; if {@code rightMargin} is less than the length of
     * {@code leftWord} and {@code rightWord} plus three (3); if leader is not a symbol
     */
    public static String insertTabLeader(String leftWord, String rightWord,
            int rightMargin, char leader) {
        if (leftWord == null) {
            throw new IllegalArgumentException("null leftWord");
        } else if (leftWord.isEmpty()) {
            throw new IllegalArgumentException("empty leftWord");
        }
        if (rightWord == null) {
            throw new IllegalArgumentException("null rightWord");
        } else if (rightWord.isEmpty()) {
            throw new IllegalArgumentException("empty rightWord");
        }
        if (leftWord.toLowerCase().contains("path")) {
            rightWord = splitOnPathSeparator(rightWord);
        } else if (rightMargin < (leftWord.length() + rightWord.length() + 2)) {
            throw new IllegalArgumentException("insufficient space to process");
        }
        if (leader != ' ' && leader != '-' && leader != '.' && leader != '_') {
            throw new IllegalArgumentException("invalid leader character");
        }

        int space = rightMargin - (leftWord.length() + rightWord.length());
        return leftWord + repeatChar(leader, space) + rightWord;
    }
    
    /**
     * Centers the given text, {@code toCenter}, within the given boundaries,
     * {@code withinBounds}. Returns the resultant string padded to the left with
     * spaces that will allow the text to appear centered. For example:
     * <pre>
     * System.out.println(StringUtils.repeat("-", 72) 
     *         + "\n" 
     *         + StringUtils.centerString("System Information", 72) 
     *         + "\n" 
     *         + StringUtils.repeat("-", 72));
     * </pre><p>
     * ...would result in the following output:
     * </p><pre>
     * ------------------------------------------------------------------------
     *                            System Information
     * ------------------------------------------------------------------------
     * </pre>
     * 
     * @param toCenter the text to be centered
     * @param withinBounds the bounds in which to center the given text
     * @return the given text padded to the left with spaces to appear centered
     */
    public static String centerString(String toCenter, int withinBounds) {
        String leftHalf = toCenter.substring(0, toCenter.length() / 2);
        
        String paddedLeft = padRight(leftHalf, withinBounds / 2);
        String accum = paddedLeft + toCenter.substring(toCenter.length() / 2);
        
        // Debugging code for tests...
        System.out.println("Returning from centerString:\n");
        System.out.println(accum);
        
        return accum;
    }
    
    private static String splitOnPathSeparator(String path) {
        return (path.replace(System.getProperty("path.separator"), "\n\t"));
    }

    private static String repeatChar(char toRepeat, int times) {
        String repeated = "";

        for (int x = 1; x <= times; x++) {
            repeated += toRepeat;
        }

        return repeated;
    }

}
