/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Utilities
 *  Class      :   NetUtilsTest.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 20, 2023
 *  Modified   :   Jan 20, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 20, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick
 */
public class NetUtilsTest {
    
    public NetUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isNetworkConnectionAvailable method, of class NetUtils.
     */
    @Test
    public void testIsNetworkConnectionAvailable() {
        System.out.println("isNetworkConnectionAvailable");
        assertTrue(NetUtils.isNetworkConnectionAvailable());
    }

    /**
     * Test of canPing method, of class NetUtils.
     */
    @Test
    public void testCanPing() {
        System.out.println("canPing");
        assertTrue(NetUtils.canPing("www.google.com"));
    }
    
}
