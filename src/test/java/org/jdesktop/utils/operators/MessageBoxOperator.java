/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Utilities
 *  Class      :   MessageBoxOperator.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 20, 2023
 *  Modified   :   Jan 20, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 20, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.utils.operators;

import java.awt.Component;
import javax.swing.JTextField;
import org.jdesktop.utils.MessageBox;
import org.netbeans.jemmy.ComponentChooser;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JTextFieldOperator;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class MessageBoxOperator extends JDialogOperator {
    
    public MessageBoxOperator () {
        super("Confirm: Heart breaking question");
    }

    /**
     * Finds JLabel in the message box and return text it displays
     * @return Text shown in the message box.
     */    
    public String getMessage() {
        JLabelOperator messageLabelOperator = new JLabelOperator(this, 0);
        return messageLabelOperator.getText();
    }
    
    /**
     * Shows a confirmation dialog with the title "Confirm Test".
     * @return the value of the button clicked
     */
    public int showConfirmDialog() {
        return MessageBox.askQuestion(null, "Will you click cancel?", "Test", true);
    }
    
    /**
     * Shows an informational dialog with the title "Information - Test"
     */
    public void showInfoDialog() {
        MessageBox.showInfo(null, "Informative, isn't it?", "Test");
    }
    
    /**
     * Shows an input dialog with the title "Provide Input - Test" and the prompt
     * "Enter your name"
     * @return 
     */
    public String showInputDialog() {
        return MessageBox.getInput(null, "Enter your name.", "Test");
    }
    
    /**
     * Shows a warning dialog with the title "Warning - Test" and the warning
     * "This is a warning"
     */
    public void showWarningDialog() {
        MessageBox.showWarning(null, "This is a warning", "Test");
    }
    
    /**
     * Shows an error dialog with the title "Error - Test" and a RuntimeException
     * as the error.
     */
    public void showErrorDialog() {
        MessageBox.showError(null, new RuntimeException("Test Error"), "Test");
    }
    
    public void provideInput(String input) {
        JTextField field = JTextFieldOperator.findJTextField(getContentPane(), new GetInputField());
        field.setText(input);
    }
    
    /**
     * Clicks the cancel button on the {@code MessageBox}.
     */
    public void doCancel() {
        JButtonOperator cancelOperator = new JButtonOperator(this, "Cancel");
        cancelOperator.push();
    }
    
    /**
     * Clicks the OK button on the {@code MessageBox}.
     */
    public void doOK() {
        JButtonOperator okOperator = new JButtonOperator(this, "OK");
        okOperator.push();
    }
    
    /**
     * Clicks the Yes button on the {@code MessageBox}.
     */
    public void doYes() {
        JButtonOperator yesOperator = new JButtonOperator(this, "Yes");
        yesOperator.push();
    }
    
    /**
     * Clicks the No button on the {@code MessageBox}.
     */
    public void doNo() {
        JButtonOperator noOperator = new JButtonOperator(this, "No");
        noOperator.push();
    }
    
    private class GetInputField implements ComponentChooser {

        @Override
        public boolean checkComponent(Component cmpnt) {
            return cmpnt instanceof JTextField;
        }

        @Override
        public String getDescription() {
            return "Finds the input field on the MessageBox";
        }
        
    }

}
