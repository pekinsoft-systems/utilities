/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Utilities
 *  Class      :   ArgumentParserTest.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 20, 2023
 *  Modified   :   Jan 20, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 20, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick
 */
public class ArgumentParserTest {
    
    private String[] args;
    
    public ArgumentParserTest() {
    }
    
    @Before
    public void setUp() {
        args = new String[] {
            "-d",
            "--version",
            "-l",
            "7"
        };        
    }
    
    @After
    public void tearDown() {
        args = null;
    }

    /**
     * Test of parse method, of class ArgumentParser.
     */
    @Test
    public void testParse() {
        System.out.println("parse");
        ArgumentParser instance = new ArgumentParser(args);
        instance.parse(args);
    }

    /**
     * Test of getArguments method, of class ArgumentParser.
     */
    @Test
    public void testGetArguments() {
        System.out.println("getArguments");
        ArgumentParser instance = new ArgumentParser(args);
        String[] expResult = args;
        String[] result = instance.getArguments();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getArgument method, of class ArgumentParser.
     */
    @Test
    public void testGetArgument() {
        System.out.println("getArgument");
        int idx = 0;
        ArgumentParser instance = new ArgumentParser(args);
        String expResult = "-d";
        String result = instance.getArgument(idx);
        assertEquals(expResult, result);
    }

    /**
     * Test of isSwitchPresent method, of class ArgumentParser.
     */
    @Test
    public void testIsSwitchPresent() {
        System.out.println("isSwitchPresent");
        String switchName = "--version";
        ArgumentParser instance = new ArgumentParser(args);
        boolean expResult = true;
        boolean result = instance.isSwitchPresent(switchName);
        assertEquals(expResult, result);
        
        expResult = false;
        switchName = "--debug";
        result = instance.isSwitchPresent(switchName);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSwitchValue method, of class ArgumentParser.
     */
    @Test
    public void testGetSwitchValue_String() {
        System.out.println("getSwitchValue");
        String switchName = "-l";
        ArgumentParser instance = new ArgumentParser(args);
        String expResult = "7";
        String result = instance.getSwitchValue(switchName);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSwitchValue method, of class ArgumentParser.
     */
    @Test
    public void testGetSwitchValue_String_String() {
        System.out.println("getSwitchValue");
        String switchName = "-k";
        String defaultValue = "hello";
        ArgumentParser instance = new ArgumentParser(args);
        String expResult = "hello";
        String result = instance.getSwitchValue(switchName, defaultValue);
        assertEquals(expResult, result);
        
        switchName = "-l";
        defaultValue = "12";
        expResult = "7";
        result = instance.getSwitchValue(switchName, defaultValue);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSwitchLongValue method, of class ArgumentParser.
     */
    @Test
    public void testGetSwitchLongValue_String() {
        System.out.println("getSwitchLongValue");
        String switchName = "-l";
        ArgumentParser instance = new ArgumentParser(args);
        Long expResult = 7L;
        Long result = instance.getSwitchLongValue(switchName);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSwitchLongValue method, of class ArgumentParser.
     */
    @Test
    public void testGetSwitchLongValue_String_Long() {
        System.out.println("getSwitchLongValue");
        String switchName = "-d";
        Long defaultValue = 12L;
        ArgumentParser instance = new ArgumentParser(args);
        Long expResult = 12L;
        Long result = instance.getSwitchLongValue(switchName, defaultValue);
        assertEquals(expResult, result);
        
        switchName = "-l";
        expResult = 7L;
        result = instance.getSwitchLongValue(switchName, defaultValue);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSwitchDoubleValue method, of class ArgumentParser.
     */
    @Test
    public void testGetSwitchDoubleValue_String() {
        System.out.println("getSwitchDoubleValue");
        String switchName = "-l";
        ArgumentParser instance = new ArgumentParser(args);
        Double expResult = 7.0d;
        Double result = instance.getSwitchDoubleValue(switchName);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSwitchDoubleValue method, of class ArgumentParser.
     */
    @Test
    public void testGetSwitchDoubleValue_String_Double() {
        System.out.println("getSwitchDoubleValue");
        String switchName = "-d";
        Double defaultValue = 12.0d;
        ArgumentParser instance = new ArgumentParser(args);
        Double expResult = 12.0d;
        Double result = instance.getSwitchDoubleValue(switchName, defaultValue);
        assertEquals(expResult, result);
        
        switchName = "-l";
        expResult = 7.0d;
        result = instance.getSwitchDoubleValue(switchName, defaultValue);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSwitchValues method, of class ArgumentParser.
     */
    @Test
    public void testGetSwitchValues() {
        System.out.println("getSwitchValues");
        String switchName = "-l";
        ArgumentParser instance = new ArgumentParser(args);
        String[] expResult = new String[] { "7" };
        String[] result = instance.getSwitchValues(switchName);
        assertArrayEquals(expResult, result);
    }
    
    public class SwitchLObject {
        private String l;
        private String value;
        public SwitchLObject() {
        }
        public String getL() {
            return l;
        }
        public void setL(String l) {
            this.l = l;
        }
        public String getValue() {
            return value;
        }
        public void setValue(String value){
            this.value = value;
        }
    }

    /**
     * REMOVED: We never use the getSwitchPOJO method anyway, as it seems to
     *          serve no real purpose. Considering removing it from the 
     *          {@code ArgumentParser} class.
     * <p>
     * Test of getSwitchPOJO method, of class ArgumentParser.</p>
     */
    @Test
    public void testGetSwitchPOJO() {
//        System.out.println("getSwitchPOJO");
//        ArgumentParser instance = new ArgumentParser(args);
//        SwitchLObject obj = new SwitchLObject();
//        obj.setL("-l");
//        obj.setValue("7");
//        Object expResult = new SwitchLObject();
//        Object result = instance.getSwitchPOJO(SwitchLObject.class);
//        assertEquals(expResult, result);
    }

    /**
     * Test of getTargets method, of class ArgumentParser.
     */
    @Test
    public void testGetTargets() {
        System.out.println("getTargets");
        ArgumentParser instance = new ArgumentParser(args);
        String[] expResult = new String[] { "7"};
        String[] result = instance.getTargets();
        assertArrayEquals(expResult, result);
    }
    
}
