/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Utilities
 *  Class      :   ColorUtilsTest.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 19, 2023
 *  Modified   :   Jan 19, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 19, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import java.awt.Color;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick
 */
public class ColorUtilsTest {
    
    public ColorUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isDarkColor method, of class ColorUtils.
     */
    @Test
    public void testIsDarkColor_3args() {
        System.out.println("isDarkColor");
        double r = 0.0;
        double g = 0.0;
        double b = 0.0;
        boolean expResult = true;
        boolean result = ColorUtils.isDarkColor(r, g, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of isDarkColor method, of class ColorUtils.
     */
    @Test
    public void testIsDarkColor_Color() {
        System.out.println("isDarkColor");
        Color color2Check = Color.white;
        boolean expResult = false;
        boolean result = ColorUtils.isDarkColor(color2Check);
        assertEquals(expResult, result);
    }

    /**
     * Test of colorDistance method, of class ColorUtils.
     */
    @Test
    public void testColorDistance_Color_Color() {
        System.out.println("colorDistance");
        Color color1 = Color.white;
        Color color2 = Color.darkGray;
        double expResult = 1.2973399909412753;
        double result = ColorUtils.colorDistance(color1, color2);
        assertEquals(expResult, result, 0);
    }

    /**
     * Test of colorDistance method, of class ColorUtils.
     */
    @Test
    public void testColorDistance_doubleArr_doubleArr() {
        System.out.println("colorDistance");
        double[] color1 = new double[] { 0.0d, 0.0d, 0.0d };
        double[] color2 = new double[] { 1.0d, 1.0d, 1.0d };
        double expResult = 1.7320508075688772;
        double result = ColorUtils.colorDistance(color1, color2);
        assertEquals(expResult, result, 0);
    }

    /**
     * Test of colorDistance method, of class ColorUtils.
     */
    @Test
    public void testColorDistance_6args() {
        System.out.println("colorDistance");
        double r1 = 0.0;
        double g1 = 0.0;
        double b1 = 0.0;
        double r2 = 1.0;
        double g2 = 1.0;
        double b2 = 1.0;
        double expResult = 1.7320508075688772;
        double result = ColorUtils.colorDistance(r1, g1, b1, r2, g2, b2);
        assertEquals(expResult, result, 0);
    }
    
}
