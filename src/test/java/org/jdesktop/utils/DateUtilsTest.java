/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Utilities
 *  Class      :   DateUtilsTest.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 19, 2023
 *  Modified   :   Jan 19, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 19, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick
 */
public class DateUtilsTest {
    
    private Calendar cal;
    private Date date;
    private java.sql.Date sqlDate;
    private Time sqlTime;
    private Timestamp sqlTimestamp;
    private LocalDate lDate;
    private LocalTime lTime;
    private LocalDateTime lDateTime;
    
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private DateTimeFormatter dtf = DateTimeFormatter.ISO_LOCAL_DATE;
    
    private final long ONE_SECOND = 60;
    private final long ONE_MINUTE = 60;
    private final long ONE_HOUR = 60;
    private final long ONE_DAY = 24;
    private final long ONE_SECOND_IN_NANOS = 1_000_000_000;
    private final long ONE_MINUTE_IN_NANOS = ONE_SECOND_IN_NANOS * 60;
    private final long ONE_HOUR_IN_NANOS = ONE_MINUTE_IN_NANOS * 60;
    private final long ONE_DAY_IN_NANOS = ONE_HOUR_IN_NANOS * 24;
    
    
    public DateUtilsTest() {
    }
    
    @Before
    public void setUp() {
        // Create a Calendar based on the current date/time.
        cal = Calendar.getInstance();
        
        // Use the Calendar object to initialize all of our other date/time objects.
        date = cal.getTime();
        
        lDate = LocalDate.ofInstant(cal.toInstant(), ZoneId.systemDefault());
        lTime = LocalTime.ofInstant(cal.toInstant(), ZoneId.systemDefault());
        lDateTime = LocalDateTime.of(lDate, lTime);
        
        sqlDate = java.sql.Date.valueOf(lDate);
        sqlTime = Time.valueOf(lTime);
        sqlTimestamp = Timestamp.valueOf(lDateTime);
    }
    
    @After
    public void tearDown() {
        sqlTimestamp = null;
        sqlTime = null;
        sqlDate = null;
        
        lDateTime = null;
        lTime = null;
        lDate = null;
        
        date = null;
        
        cal = null;
    }
    
    private LocalDateTime getIncrementedTime(int calendarField, int amount) {
        Calendar inc = Calendar.getInstance();
        inc.add(calendarField, amount);
        return LocalDateTime.ofInstant(inc.toInstant(), ZoneId.systemDefault());
    }

    /**
     * Test of getCurrentYear method, of class DateUtils.
     */
    @Test
    public void testGetCurrentYear() {
        System.out.println("getCurrentYear");
        long expResult = 2023;
        long result = DateUtils.getCurrentYear();
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateDifferenceInNanos method, of class DateUtils.
     */
    @Test
    public void testCalculateDifferenceInNanos_LocalDateTime_LocalDateTime() {
        System.out.println("calculateDifferenceInNanos");
        LocalDateTime start = lDateTime;
        LocalDateTime end = getIncrementedTime(Calendar.HOUR_OF_DAY, 3);
        long expResult = 3 * ONE_HOUR_IN_NANOS;
        long result = DateUtils.calculateDifferenceInNanos(start, end);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateDifferenceInNanos method, of class DateUtils.
     */
    @Test
    public void testCalculateDifferenceInNanos_Time_Time() {
        System.out.println("calculateDifferenceInNanos");
        Time start = sqlTime;
        LocalDateTime tmp1 = getIncrementedTime(Calendar.MINUTE, 15);
        LocalTime tmp = LocalTime.of(tmp1.getHour(), tmp1.getMinute(), tmp1.getSecond(), tmp1.getNano());
        Time end = Time.valueOf(tmp);
        long expResult = 15 * ONE_MINUTE_IN_NANOS;
        long result = DateUtils.calculateDifferenceInNanos(start, end);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateDifferenceInSeconds method, of class DateUtils.
     */
    @Test
    public void testCalculateDifferenceInSeconds() {
        System.out.println("calculateDifferenceInSeconds");
        LocalDateTime start = lDateTime;
        LocalDateTime end = getIncrementedTime(Calendar.MINUTE, 3);
        double expResult = 3 * ONE_SECOND;
        double result = DateUtils.calculateDifferenceInSeconds(start, end);
        assertEquals(expResult, result, 0);
    }

    /**
     * Test of calculateDifferenceInMinutes method, of class DateUtils.
     */
    @Test
    public void testCalculateDifferenceInMinutes() {
        System.out.println("calculateDifferenceInMinutes");
        LocalDateTime ago = lDateTime;
        LocalDateTime later = getIncrementedTime(Calendar.MINUTE, 5);
        double expResult = 5;
        double result = DateUtils.calculateDifferenceInMinutes(ago, later);
        assertEquals(expResult, result, 0);
    }

    /**
     * Test of calculateDifferenceInHours method, of class DateUtils.
     */
    @Test
    public void testCalculateDifferenceInHours() {
        System.out.println("calculateDifferenceInHours");
        LocalDateTime ago = lDateTime;
        LocalDateTime later = getIncrementedTime(Calendar.HOUR, 4);
        double expResult = 4;
        double result = DateUtils.calculateDifferenceInHours(ago, later);
        assertEquals(expResult, result, 0);
    }

    /**
     * Test of calculateDifferenceInDays method, of class DateUtils.
     */
    @Test
    public void testCalculateDifferenceInDays() {
        System.out.println("calculateDifferenceInDays");
        LocalDateTime ago = lDateTime;
        LocalDateTime later = getIncrementedTime(Calendar.DAY_OF_MONTH, 4);
        double expResult = 4;
        double result = DateUtils.calculateDifferenceInDays(ago, later);
        assertEquals(expResult, result, 0);
    }

    /**
     * Test of convertDate2LocalDate method, of class DateUtils.
     */
    @Test
    public void testConvertDate2LocalDate() {
        System.out.println("convertDate2LocalDate");
        Date date2Convert = date;
        LocalDate expResult = lDate;
        LocalDate result = DateUtils.convertDate2LocalDate(date2Convert);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertLocalDate2Date method, of class DateUtils.
     */
    @Test
    public void testConvertLocalDate2Date() {
        System.out.println("convertLocalDate2Date");
        LocalDate date2Convert = lDate;
        Date expResult = date;
        Date result = DateUtils.convertLocalDate2Date(date2Convert);
        assertEquals(sdf.format(expResult), sdf.format(result));
    }

    /**
     * Test of convertDate2LocalTime method, of class DateUtils.
     */
    @Test
    public void testConvertDate2LocalTime() {
        System.out.println("convertDate2LocalTime");
        Date date2Convert = date;
        LocalTime expResult = lTime;
        LocalTime result = DateUtils.convertDate2LocalTime(date2Convert);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertDate2SqlDate method, of class DateUtils.
     */
    @Test
    public void testConvertDate2SqlDate() {
        System.out.println("convertDate2SqlDate");
        Date date2Convert = date;
        java.sql.Date expResult = sqlDate;
        java.sql.Date result = DateUtils.convertDate2SqlDate(date2Convert);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertDate2SqlTime method, of class DateUtils.
     */
    @Test
    public void testConvertDate2SqlTime() {
        System.out.println("convertDate2SqlTime");
        Date date2Convert = date;
        Time expResult = sqlTime;
        Time result = DateUtils.convertDate2SqlTime(date2Convert);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertDate2SqlTimestamp method, of class DateUtils.
     */
    @Test
    public void testConvertDate2SqlTimestamp() {
        System.out.println("convertDate2SqlTimestamp");
        Date date2convert = date;
        Timestamp expResult = sqlTimestamp;
        Timestamp result = DateUtils.convertDate2SqlTimestamp(date2convert);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertLocalTime2Date method, of class DateUtils.
     */
    @Test
    public void testConvertLocalTime2Date() {
        System.out.println("convertLocalTime2Date");
        LocalTime time2Convert = lTime;
        Date expResult = date;
        Date result = DateUtils.convertLocalTime2Date(time2Convert);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertDate2LocalDateTime method, of class DateUtils.
     */
    @Test
    public void testConvertDate2LocalDateTime() {
        System.out.println("convertDate2LocalDateTime");
        Date date2Convert = date;
        LocalDateTime expResult = lDateTime;
        LocalDateTime result = DateUtils.convertDate2LocalDateTime(date2Convert);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertLocalDateTime2Date method, of class DateUtils.
     */
    @Test
    public void testConvertLocalDateTime2Date() {
        System.out.println("convertLocalDateTime2Date");
        LocalDateTime date2Convert = lDateTime;
        Date expResult = date;
        Date result = DateUtils.convertLocalDateTime2Date(date2Convert);
        assertEquals(expResult, result);
    }
    
}
