/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Utilities
 *  Class      :   MessageBoxTest.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 20, 2023
 *  Modified   :   Jan 20, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 20, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.utils;

import org.jdesktop.utils.operators.MessageBoxOperator;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.jemmy.EventTool;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.util.PNGEncoder;

/**
 * A simple test using Jemmy for the {@code MessageBox} class.
 * <p>
 * <em><strong>NOTE</strong>: I have commented out all JUnit Testing Framework
 * annotations until such time as we can get this working. I want the pipeline
 * to successfully complete on the GitLab repository.</em></p>
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class MessageBoxTest {
    
    private static MessageBoxOperator msgBoxOperator;
    
    public MessageBoxTest () {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
        java.awt.EventQueue.invokeLater(() -> {
            MessageBox.askQuestion(null, "Do you still love me?", "Heart breaking question", true);
        });
        msgBoxOperator = new MessageBoxOperator();
    }
    
   @AfterClass
    public static void tearDownClass() {
        msgBoxOperator = null;
    }
    
    @Test
    public void testConfirmMessage() {
        String message = msgBoxOperator.getMessage();
        assertEquals("Heart breaking question asked.", "Do you still love me?", message);
    }
    
    @Test
    public void testYesButton() {
        msgBoxOperator.doYes();
    }
    
//    @Test
    public void testConfirmDialogNo() {
        int clicked = msgBoxOperator.showConfirmDialog();
        new EventTool().waitNoEvent(1000);
        msgBoxOperator.doNo();
        PNGEncoder.captureScreen("ConfirmDialogNo.png", PNGEncoder.COLOR_MODE);
        
        Assert.assertTrue(clicked == MessageBox.NO_OPTION);
    }
    
//    @Test
    public void testConfirmDialogYes() {
        int clicked = msgBoxOperator.showConfirmDialog();
        new EventTool().waitNoEvent(1000);
        msgBoxOperator.doYes();
        PNGEncoder.captureScreen("ConfirmDialogYes.png", PNGEncoder.COLOR_MODE);
        
        Assert.assertTrue(clicked == MessageBox.YES_OPTION);
    }
    
//    @Test
    public void testShowInfo() {
        msgBoxOperator.showInfoDialog();
        new EventTool().waitNoEvent(1000);
        msgBoxOperator.doOK();
        PNGEncoder.captureScreen("ShowInfoOK.png", PNGEncoder.COLOR_MODE);
    }
    
//    @Test
    public void testShowWarning() {
        msgBoxOperator.showWarningDialog();
        new EventTool().waitNoEvent(1000);
        msgBoxOperator.doOK();
        PNGEncoder.captureScreen("ShowWarningOK.png", PNGEncoder.COLOR_MODE);
    }
    
//    @Test
    public void testShowError() {
        msgBoxOperator.showErrorDialog();
        new EventTool().waitNoEvent(1000);
        msgBoxOperator.doOK();
        PNGEncoder.captureScreen("ShowErrorOK.png", PNGEncoder.COLOR_MODE);
    }
    
//    @Test
    public void testGetInputOK() {
        String input = msgBoxOperator.showInputDialog();
        msgBoxOperator.provideInput("John Dough");
        pushButtonInDialog("Provide Input - Test Input", "OK");
        PNGEncoder.captureScreen("GetInputOK.png", PNGEncoder.COLOR_MODE);
        Assert.assertTrue("John Dough".equals(input));
    }
    
//    @Test
    public void testGetInputCancel() {
        String input = MessageBox.getInput(null, "Enter your name", "Test Input");
        pushButtonInDialog("Provide Input - Test Input", "Cancel");
        PNGEncoder.captureScreen("GetInputCancel.png", PNGEncoder.COLOR_MODE);
        Assert.assertTrue(input == null || input.trim().isEmpty());
    }
    
    private void pushButtonInDialog(String dialogTitle, String buttonName) {
        JDialogOperator dialog = new JDialogOperator(dialogTitle);
        JButtonOperator button = new JButtonOperator(dialog, buttonName);
        button.push();
    }

}
